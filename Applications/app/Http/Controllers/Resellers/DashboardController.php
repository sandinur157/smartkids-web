<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Cart;
use Auth;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $request->session()->put('cart', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->get());
        $request->session()->put('cartSum', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->sum('total'));

        return view('resellers.dashboard.dashboard');
    }
}
