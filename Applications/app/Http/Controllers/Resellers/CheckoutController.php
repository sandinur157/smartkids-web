<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Billing_address;
use App\Product_stock;
use App\Payment;
use App\Order;
use App\Cart;

use Carbon\Carbon;
use Storage;
use Image;
use Auth;

class CheckoutController extends Controller
{
    public function index()
    {
        $carts = Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->get();
        $billing_address = Billing_address::where('id_reseller', Auth::guard('reseller')->user()->id)->first();

        return view("resellers.checkout.checkout", compact('carts', 'billing_address'));
    }

    public function store(Request $request)
    {
        Order::create([
            'reseller_id' => Auth::guard('reseller')->user()->id,
            'note' => $request->note,
            'total_item' => $request->total_item,
            'total_price' => $request->total_price,
        ]);

        $id = Order::where('reseller_id', Auth::guard('reseller')->user()->id)
                        ->orderBy('created_at', 'DESC')->first();

        $request->session()->forget('cart');
        $request->session()->forget('cartSum');

        return redirect()->route('reseller.orderReceived.get', $id);
    }

    public function orderReceived(Request $request)
    {
        $id = last(request()->segments());
        $order = Order::with('reseller')->where('id', $id)->first();
        
        $date = date('d F Y', strtotime($order->created_at));
        
        return view("resellers.checkout.orderReceived", compact('order', 'date'));
    }

    public function konfirmasiPembayaran(Request $request)
    {
        return view("resellers.checkout.konfirmasiPembayaran");
    }

    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/payment')) {
            Storage::disk('public')->makeDirectory('uploads/payment');
        }
        
        $image = Image::make($image)->resize(400, 400)->save();
        Storage::disk('public')->put('uploads/payment/'. $name, $image);
        
        return $name;
    }

    public function konfirmasiPembayaranPost(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
            'amount' => 'required',
            'bank' => 'required',
            'photo' => 'required|mimes:jpeg,jpg,png,bmp',
        ]);

        $image = $this->_saveFile("$request->name", $request->file("photo"));
        
    	Payment::create([
    		'id_reseller' => Auth::guard('reseller')->user()->id,
    		'order_id' => $id,
    		'name' => $request->name,
    		'amount' => $request->amount,
    		'bank_name' => $request->bank,
    		'photo' => $image,
        ]);

        $order = Order::where('id', $id)->first();
        $order->update([
            'status' => "On Hold"
        ]);
        
        return redirect()->route('reseller.order.get');
    }
}
