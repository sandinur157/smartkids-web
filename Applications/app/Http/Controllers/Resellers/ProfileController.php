<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Reseller;

use Validator;
use Auth;
use Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $reseller = Reseller::where('id', Auth::guard('reseller')->user()->id)->first();

        return view('resellers.profile.profile', compact('reseller'));
    }

    public function update(Request $request)
    {
        $messages = [
            'digits' => 'Isian postal code harus berdigit 5 angka.'
        ];

    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'min:3|max:30',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'postal_code' => 'required|numeric|digits:5',
            'phone' => 'required|numeric',
            'bank_name' => 'required|max:30',
            'bank_account' => 'required|numeric',
            'bank_account_name' => 'required|max:50',
            'partners_name' => 'required|max:50',
            'office_address' => 'max:50',
            'phone_recruiter' => 'required|numeric',
            'email_recruiter' => 'email|nullable',
        ], $messages);

        // validation
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $reseller = Reseller::where('id', Auth::guard('reseller')->user()->id)->first();
        $reseller->update([
    		'email' => $request->email,
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'address' => $request->address,
    		'city' => $request->city,
    		'postal_code' => $request->postal_code,
    		'phone' => $request->phone,
    		'bank_name' => $request->bank_name,
    		'bank_account' => $request->bank_account,
    		'bank_account_name' => $request->bank_account_name,
    		'partners_name' => $request->partners_name,
    		'office_address' => $request->office_address,
    		'phone_recruiter' => $request->phone_recruiter,
    		'email_recruiter' => $request->email_recruiter,
        ]);

        return response()->json([
            'message' => 'Profile berhasil diubah.'
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'new_password' => 'min:5|max:30'
        ]);

        // validation
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        if (!(Hash::check($request->password, Auth::guard('reseller')->user()->password))) {
            return response()->json(['password' => ['Password salah']], 401);
        }else if(strcmp($request->password, $request->new_password) == 0) {
            return response()->json(['new_password' => ['Password lama dan baru tidak boleh sesuai']], 401);
        }else if(strcmp($request->new_password, $request->password_confirmation) != 0) {
            return response()->json(['password_confirmation' => ['Konfirmasi password tidak sesuai']], 401);
        }

        $reseller = Reseller::where('id', Auth::guard('reseller')->user()->id);
        $reseller->update([
            'password' => bcrypt($request->new_password)
        ]);

        return response()->json([
            'message' => 'Password berhasil diubah.'
        ], 200);
    }
}
