<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use App\Product_stock;
use App\Cart;

use Auth;

class CartController extends Controller
{
    public function store(Request $request)
    {
        $product_stock = Product_stock::where('product_id', $request->id_product)->first();
        
        if(!$product_stock) {
            return response()->json([
                'message' => 'Stok produk tidak ditemukan'
            ], 404);
        }

        // check amount
        if($request->qty <= $product_stock->amount && $request->qty > 0) {

            // TOTAL
            $product = Product::where('id', $request->id_product)->first();
            $total = $product->nominal_price * $request->qty;

            $cart = Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)
                        ->where('id_product', $request->id_product)
                        ->first();
            
            if($cart == null) {
                Cart::create([
                    "id_reseller" => Auth::guard('reseller')->user()->id,
                    "id_product" => $request->id_product,
                    "qty" => $request->qty,
                    "total" => $total,
                ]);
            }else{
                $cart->update([
                    "qty" => $request->qty,
                    "total" => $total,
                ]);
            }

            // ADD TO SESSION
            $cartList = $request->session()->put('cart', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->get());
            $sum = $request->session()->put('cartSum', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->sum('total'));
            
            if ($request->ajax()) {
                return view('resellers.partials.cartList', compact('cartList', 'sum'))->render();
            }

        }else{
            return response()->json([
                'message' => 'Stok tidak tersedia'
            ], 422);
        }

        return "Berhasil";
    }

    public function delete(Request $request)
    {
        try {
            $cart = Cart::where('id', $request->id_cart)->first();
            $cart->delete();

            // ADD TO SESSION
            $cartList = $request->session()->put('cart', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->get());
            $sum = $request->session()->put('cartSum', Cart::with('product')->where('id_reseller', Auth::guard('reseller')->user()->id)->sum('total'));
            
            if ($request->ajax()) {
                return view('resellers.partials.cartList', compact('cartList', 'sum'))->render();
            }

            return response()->json([
                'message' => 'Berhasil menghapus data'
            ], 200);

        } catch (Exception $e) {
            return response()->json([
                'message' => 'Gagal menghapus data'
            ], 500);
        }
    }
}
