<?php

namespace App\Http\Controllers\Resellers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Shipping_address;
use App\Billing_address;
use App\Reseller;

use Carbon\Carbon;
use Storage;
use Image;
use Auth;

class AuthController extends Controller
{
    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/resellers')) {
            Storage::disk('public')->makeDirectory('uploads/resellers');
        }
        
        $image = Image::make($image)->resize(400, 400)->save();
        Storage::disk('public')->put('uploads/resellers/'. $name, $image);
        
        return $name;
    }

    public function _storeBillingAddress($first_name, $last_name, $street_address, $postal_code, $city, $phone, $email)
    {
        $user = Reseller::latest()->first();

        Billing_address::create([
            'id_reseller' => $user->id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'street_address' => $street_address,
            'postal_code' => $postal_code,
            'city' => $city,
            'phone' => $phone,
            'email' => $email
        ], 201);
    }

    public function _storeShippingAddress($first_name, $last_name, $street_address, $city, $postal_code)
    {
        $user = Reseller::latest()->first();

        Shipping_address::create([
            'id_reseller' => $user->id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'street_address' => $street_address,
            'city' => $city,
            'postal_code' => $postal_code
        ], 201);
    }

    public function getLogin()
    {
        return view('resellers.auth.login');
    }

    public function postLogin(Request $request)
    {
        $remember = false;
        if($request->remember == 'on') $remember = true;

        $identity = request()->input('identity');

        if(!\Auth::guard('reseller')->attempt([filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username' => $identity, 'password' => $request->password], $remember)) {
            return redirect()->back()->with('danger', 'Email atau Password salah');
        }

        if(\Auth::guard('reseller')->user()->active == 0) {
            \Auth::guard('reseller')->logout();
            return redirect()->back()->with('danger', 'Akun anda belum di verifikasi Admin');
        }

        return redirect()->route('reseller.dashboard.get');
    }

    public function getRegister()
    {
        return view('resellers.auth.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:30',
            'last_name' => 'max:30',
            'username' => 'required|unique:resellers|max:30',
            'email' => 'required|unique:resellers|email',
            'password' => 'required|min:5|confirmed',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'postal_code' => 'numeric|digits:5',
            'phone' => 'required|numeric',
            'bank_name' => 'required|max:30',
            'bank_account' => 'required|numeric',
            'bank_account_name' => 'required|max:50',
            'ktp' => "required|mimes:jpeg,jpg,png,bmp",
            'partners_name' => 'required|max:50',
            'office_address' => 'max:50',
            'phone_recruiter' => 'required|numeric',
            'email_recruiter' => 'unique:resellers|email|nullable',
        ]);

        $image = $this->_saveFile("$request->first_name . '-' . $request->last_name", $request->file("ktp"));
        
    	Reseller::create([
    		'username' => $request->username,
    		'email' => $request->email,
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'password' => bcrypt($request->password),
    		'address' => $request->address,
    		'city' => $request->city,
    		'postal_code' => $request->postal_code,
    		'phone' => $request->phone,
    		'bank_name' => $request->bank_name,
    		'bank_account' => $request->bank_account,
    		'bank_account_name' => $request->bank_account_name,
    		'ktp' => $image,
    		'partners_name' => $request->partners_name,
    		'office_address' => $request->office_address,
    		'phone_recruiter' => $request->phone_recruiter,
    		'email_recruiter' => $request->email_recruiter,
    	]);

        // storeBillingAddress
        $this->_storeBillingAddress($request->first_name, $request->last_name, $request->address, $request->postal_code, $request->city, $request->phone, $request->email);
        // storeShippingAddress
        $this->_storeShippingAddress($request->first_name, $request->last_name, $request->address, $request->city, $request->postal_code);

    	return back()->with('success', "Terima kasih, Akun anda akan segera di proses");
    }

    public function logout()
    {
        \Auth::guard('reseller')->logout();
        return redirect()->route('reseller.login.get');
    }
}
