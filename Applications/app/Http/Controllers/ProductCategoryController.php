<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_category;

class ProductCategoryController extends Controller
{
    public function index()
    {
        $categories = Product_category::with('products')->latest()->get();
    	return view('product_categories.index', compact('categories'));
    }

    public function show($slug)
    {
        $products = Product_category::with('products')->where('slug', $slug)->first();
        $product_categories = Product_category::latest()->get();

        return view('products.index', compact('products', 'product_categories', 'slug'));
    }

    public function edit($id)
    {
        $category = Product_category::find($id);
        echo json_encode($category);
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
            'name' => 'required|string|min:4|max:25|unique:product_categories',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Product_category::create([
                'name' => $request->name,
                'slug' => str_slug($request->name),
                'description' => $request->description
            ]);
            return back()->with('message', [
                'title' => 'Kategori berhasil ditambahkan.'
            ]);

        } catch (Exception $e) {
            return back()->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(), [
            'name' => 'required|string|min:4|max:25',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Product_category::find($id)->update([
                'name' => $request->name,
                'slug' => str_slug($request->name),
                'description' => $request->description
            ]);

            return redirect()->route('product_category.index')->with('message', [
                'title' => 'Kategori berhasil diubah.'
            ]);
        } catch (Exception $e) {
            return redirect()->route('product_category.index')->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
    	$category = Product_category::find($id)->delete();
        return back()->with('message', [
            'title' => 'Kategori berhasil dihapus.'
        ]);
    }
}
