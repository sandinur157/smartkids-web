<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Product_category;
use App\Product_attribute;
use App\Product_stock;
use Auth;
use Carbon\Carbon;
use Storage;
use Image;
use DB;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->get();   
        $product_categories = Product_category::latest()->get();
    	return view('products.index', compact('products', 'product_categories'));
    }

    public function show($slug)
    {
        $product_categories = Product_category::latest()->get();
        return view('products.show', compact('product_categories'));
    }

    public function create()
    {
        $product_categories = Product_category::latest()->get();
        $product_attributes = Product_attribute::latest()->get();
    	return view('products.create', compact('product_categories', 'product_attributes'));
    }

    public function edit($id)
    {
        $product = Product::with('product_categories')->find($id);
        $product_categories = Product_category::latest()->get();
        $product_attributes = Product_attribute::latest()->get();
        return view('products.edit', compact('product', 'product_categories', 'product_attributes'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'normal_price' => 'required',
            'salling_price' => 'required',
            'sku' => 'required|string',
            'manage_stock' => 'nullable|integer',
            'amount' => 'nullable|integer',
            'allow_backorder' => 'nullable|string',
            'limit_stock' => 'nullable|integer',
            'sold_individually' => 'nullable|integer',
            'weight' => 'nullable|integer',
            'length' => 'nullable|integer',
            'width' => 'nullable|integer',
            'height' => 'nullable|integer',
            'shipping_class' => 'nullable|string',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $normal_price  = explode(',', $request->normal_price);
        $normal_price  = implode('', $normal_price);
        $salling_price = explode(',', $request->salling_price);
        $salling_price = implode('', $salling_price);

        try {
            $product = new Product;
            $product->name              = $request->name;
            $product->description       = $request->description;
            $product->normal_price      = $normal_price;
            $product->salling_price     = $salling_price;
            $product->sku               = $request->sku;
            $product->manage_stock      = $request->manage_stock;
            $product->amount            = $request->amount;
            $product->allow_backorder   = $request->allow_backorder;
            $product->limit_stock       = $request->limit_stock;
            $product->sold_individually = $request->sold_individually;
            $product->weight            = $request->weight;
            $product->length            = $request->length;
            $product->width             = $request->width;
            $product->height            = $request->height;
            $product->shipping_class   = $request->shipping_class;
            $product->user_id           = Auth::id();

            if($request->hasFile('photo')) {
                $photo = $this->_saveFile($request->name, $request->file('photo'));
            } else $photo = null;

            $product->photo = $photo;
            $product->save();

            // set category product
            foreach ($request->product_categories as $product_category_id) {
               $category_product = DB::table('product_product_category')->insert([
                    'product_id' => $product->id,
                    'Product_category_id' => $product_category_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            // sync product stock
            $product_stock = Product_stock::create([
                'date_changed' => date('d/m/Y', strtotime(Carbon::now())),
                'product_id' => $product->id,
                'amount' => $product->amount,
                'description' => 'Tambah produk baru'
            ]);


            return back()->with('message', [
                'title' => 'Produk berhasil ditambahkan.'
            ]);

        } catch (Exception $e) {
            return back()->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/products')) {
            Storage::disk('public')->makeDirectory('uploads/products');
        }
        
        $image = Image::make($image)->save();
        Storage::disk('public')->put('uploads/products/'. $name, $image);
        
        return $name;
    }

    public function update(Product $product,  Request $request)
    {
    	$this->validate($request, [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'normal_price' => 'required',
            'salling_price' => 'required',
            'sku' => 'required|string',
            'manage_stock' => 'nullable|integer',
            'amount' => 'nullable|integer',
            'allow_backorder' => 'nullable|string',
            'limit_stock' => 'nullable|integer',
            'sold_individually' => 'nullable|integer',
            'weight' => 'nullable|integer',
            'length' => 'nullable|integer',
            'width' => 'nullable|integer',
            'height' => 'nullable|integer',
            'shipping_class' => 'nullable|string',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $normal_price  = explode(',', $request->normal_price);
        $normal_price  = implode('', $normal_price);
        $salling_price = explode(',', $request->salling_price);
        $salling_price = implode('', $salling_price);

        try {
            $product->name              = $request->name;
            $product->description       = $request->description;
            $product->normal_price      = $normal_price;
            $product->salling_price     = $salling_price;
            $product->sku               = $request->sku;
            $product->manage_stock      = $request->manage_stock;
            $product->amount            = $request->amount;
            $product->allow_backorder   = $request->allow_backorder;
            $product->limit_stock       = $request->limit_stock;
            $product->sold_individually = $request->sold_individually;
            $product->weight            = $request->weight;
            $product->length            = $request->length;
            $product->width             = $request->width;
            $product->height            = $request->height;
            $product->shipping_class    = $request->shipping_class;
            $product->user_id           = Auth::id();

            if($request->hasFile('photo')) {
                $photo = $this->_saveFile($request->name, $request->file('photo'));
            } else $photo = $product->photo;

            $product->photo = $photo;
            $product->update();

            // set category product
            // foreach ($request->product_categories as $product_category_id) {
            //     $category_product = DB::table('product_product_category')->get();

            //     if (!in_array($product_category_id, $category_product)) {
            //         $category_product = DB::table('product_product_category')->create([
            //             'product_id' => $product->id,
            //             'Product_category_id' => $product_category_id,
            //             'updated_at' => Carbon::now()
            //         ]);
            //     }

            // }

            foreach ($request->product_categories as $product_category_id) {
                $category_product = DB::table('product_product_category')->where('product_id', $product->id)->delete();
                $category_product = DB::table('product_product_category')->insert([
                    'product_id' => $product->id,
                    'Product_category_id' => $product_category_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            // sync product stock
            $product_stock = Product_stock::where('product_id', $product->id)->first();
            if (!empty($product_stock)) {
                    $product_stock->update([
                    'date_changed' => date('d/m/Y', strtotime(Carbon::now())),
                    'product_id' => $product->id,
                    'amount' => $product->amount
                ]);
            } else {
                $product_stock = Product_stock::create([
                    'date_changed' => date('d/m/Y', strtotime(Carbon::now())),
                    'product_id' => $product->id,
                    'amount' => $product->amount,
                    'description' => 'Tambah produk baru'
                ]);
            }
            

            return back()->with('message', [
                'title' => 'Produk berhasil diupdate.'
            ]);

        } catch (Exception $e) {
            return back()->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Product $product)
    {
        // sync product stock
        $product_stock = Product_stock::where('product_id', $product->id)->first();
        
        if (!empty($product_stock)) {
            $product_stock->delete();
        }

        isset($product->photo) ? Storage::disk('public')->delete('uploads/users/'. $user->photo) : '';
    	$product->delete();

        return back()->with('message', [
            'title' => 'Produk berhasil dihapus.'
        ]);
    }

    public function history_variant($varian_id)
    {
        return view('products.history');
    }
}
