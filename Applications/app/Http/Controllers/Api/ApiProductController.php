<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product_category;
use App\Product;

class ApiProductController extends Controller
{
	private $paginate = 16;

	public function __construct()
	{
		return auth()->shouldUse('reseller');
	}

    public function index(Request $request)
    {
        $products = Product::latest('created_at')->paginate($this->paginate);
        $categories = Product_category::all();

        if ($request->ajax()) {
            return view('resellers.partials.paginate', compact('products'))->render();
        }

        return response()->json(compact('products', 'categories'));
    }

	// public function index()
	// {
	// 	$products = Product::with('product_category')->latest()->paginate(12);
	// 	$product_categories = Product_category::latest()->get();

	// 	return response()->json(compact('products', 'product_categories'));
	// }

	public function show($id)
    {
        $product = Product::where('id', $id)->with('product_category')->first();
        return response()->json(compact('product'));
    }
}
