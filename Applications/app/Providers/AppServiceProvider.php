<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Schema;
use Blade;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Blade::component('components.card', 'card');
        Blade::component('components.table', 'table');
        Blade::component('components.modal', 'modal');
        // View::composer('layouts.partials.nav', function($view) {
        //     $view->with('print_spj_today', \App\Bus_status::with('bus')->where('tanggal_pinjam', Carbon::today()->addDay(1))->get());
        // });

        // Directive
        Blade::directive('currency', function ($expression) {
            return "Rp. <?php echo number_format($expression, 0, ',', '.'); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
