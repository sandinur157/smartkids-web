<?php

namespace App;

use App\Model;

class Product_attribute extends Model
{
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
