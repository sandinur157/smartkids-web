@extends('layouts.auth')

@section('title')
    Login - Administrator
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo mb-0">
        <p class="mb-0"><b>Login</b> Administrator</p>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg mb-3">
                <img src="{{ asset('/images/logo.png') }}" alt="" class="img-fluid" width="200">
            </p>
            <form action="" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input type="text" class="form-control 
                        {{ $errors->has('email') ? 'is-invalid' : '' }}
                        {{ $errors->has('username') ? 'is-invalid' : '' }}
                    " placeholder="Username / Email." name="identity" value="{{ old('identity') }}" required autofocus>
                    
                    <span class="fa fa-envelope form-control-feedback text-muted">
                        @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                        @endif

                        @if ($errors->has('username'))
                            {{ $errors->first('username') }}
                        @endif
                    </span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') }}">
                    <span class="fas fa-lock form-control-feedback text-muted">
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                        @endif
                    </span>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-blue">
                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <button class="btn btn-primary btn-block">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@endsection