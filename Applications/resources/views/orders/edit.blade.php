@extends('layouts.master')

@section('title', 'Edit Produk')

@push('css')
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.css') }}">
    <style>
        .form-control.fc_md, .btn_md { 
            padding: 0.5rem 1rem;
            font-size: 1rem;
            line-height: 1;
        }

        .table label {
            font-size: .7em;
            text-transform: uppercase;
            color: #6c757d;
        }

        .table td {
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-12">
        @card
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="code" id="code" class="form-control fc_md" required>
                <div class="form-group row">
                    <div class="col-md-8">
                        <label for="name">Nama Produk</label>
                        <input type="text" name="name" id="name" class="form-control fc_md" required>
                    </div>
                    <div class="col-md-4">
                        <label for="product_category_id">Kategori</label>
                        <select name="product_category_id" id="product_category_id" class="form-control fc_md" required>
                            <option>-- Pilih Kategori --</option>
                            @foreach ($product_categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                            <option value="uncategorized">Uncategorized</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="description" rows="3" class="form-control"></textarea>
                </div>
                <div class="form-group row">
                    <div class="col-lg-2 col-4">
                        <label for="discount">Diskon</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control fc_md" id="discount" name="discount" aria-label="Text input with checkbox">
                            <div class="input-group-append">
                                <div class="input-group-text bg-white">
                                    %
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-8 pl-5">
                        <label for="varian">Kategori Varian</label> 
                        <br>
                        <input type="checkbox" name="size" id="size" checked> <label for="size" class="font-weight-normal mr-3">Ukuran</label>
                        <input type="checkbox" name="color" id="color" checked> <label for="color" class="font-weight-normal">Warna</label>
                    </div>
                </div>
                <table class="table table-sm table-responsive">
                    <thead class="bg-light">
                        <th class="border-left">Foto Produk</th>
                        <th>Spesifikasi</th>
                        <th>Harga Beli</th>
                        <th>Harga Jual</th>
                        <th>Varian</th>
                        <th class="border-right">Stok</th>
                    </thead>

                    <tbody>
                        <td class="border-left">
                            <img src="https://via.placeholder.com/100.png" alt="" class="img-circle mt-1">
                            <input type="file" name="photo" class="form-control form-control-sm mt-1">
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="sku" class="font-weight-normal">SKU</label>
                                <input type="text" class="form-control" name="sku" id="sku">
                            </div>
                            <div class="form-group">
                                <label for="wight" class="font-weight-normal">Berat</label>
                                <input type="text" class="form-control" name="wight" id="wight">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="purchase_price" style="visibility: hidden;">purchase_price</label>
                                <input type="text" class="form-control" name="purchase_price" id="purchase_price">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="nominal_price" class="font-weight-normal">Harga Normal</label>
                                <input type="text" class="form-control" name="nominal_price" id="nominal_price">
                            </div>
                            <div class="form-group">
                                <label for="reseller_price" class="font-weight-normal">Harga Reseller</label>
                                <input type="text" class="form-control" name="reseller_price" id="reseller_price">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="size" class="font-weight-normal">Ukuran</label>
                                <input type="text" class="form-control" name="size" id="size">
                            </div>
                            <div class="form-group">
                                <label for="color" class="font-weight-normal">Warna</label>
                                <input type="text" class="form-control" name="color" id="color">
                            </div>
                        </td>
                        <td class="border-right">
                            <div class="form-group">
                                <label for="stock" style="visibility: hidden;">stock</label>
                                <input type="text" class="form-control" name="stock" id="stock">
                            </div>
                        </td>
                    </tbody>
                </table>
                
                <div class="row">
                <div class="col-lg-7">
                    <div class="form-group">
                        <label for="whosale_price">Harga Grosir</label>
                        <table class="table table-sm">
                            <thead class="bg-light">
                                <th class="border-left">Rentang</th>
                                <th class="border-right">Harga Satuan</th>
                            </thead>
                            <tbody>
                                @for ($i = 0; $i < 5; $i++)
                                    <tr>
                                        <td class="border-left">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control border-right-0 form-control-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text bg-white" id="">s/d</span>
                                                </div>
                                                <input type="text" class="form-control form-control-sm">
                                            </div>
                                        </td>
                                        <td class="border-right">
                                            <input type="text" class="form-control" name="satuan">
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-lg-5">
                    <span class="d-block"><strong>Keterangan:</strong></span> 
                    <span class="d-block">Rentang jumlah item dimulai dari 2 (jika kurang tidak bisa digunakan)</span>

                    <br>
                    <span class="d-block"><strong>Contoh:</strong></span>
                    <span class="d-block">Rentang: 2 - 10, Harga: 10000</span> 
                    <span class="d-block">Rentang: 11 - 20, Harga: 9500</span>
                </div>
                </div>
            </form>
            @slot('footer')
                <button class="btn btn-md btn-primary"><i class="fas fa-save"></i> Simpan Poduk</button>
            @endslot
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
    <script src="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>

        $(function () {
            $('textarea').summernote({
                'height': 150,
            })

            $('span:not(.brand-text)').addClass('text-muted')

            $('select').select2()
            $('[type=checkbox]').prop('indeterminate', true)
        })
    </script>
@endpush