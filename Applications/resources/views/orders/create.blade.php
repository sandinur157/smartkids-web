@extends('layouts.master')

@section('title', 'Order')

@push('css')
    <style>
        .form-control.fc_md, .btn_md { 
            padding: 0.5rem 1rem;
            font-size: 1rem;
            line-height: 1;
        }
        
        .row_header td {
            font-size: .8em;
        }
        .row_content td {
            font-weight: bold;
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Order</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-4">
        @card
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="code" id="code" class="form-control fc_md">
                <div class="form-group">
                    <label for="name">Nama Pemesan</label>
                    <input type="text" name="name" id="name" class="form-control fc_md" required placeholder="Cari Customer">
                </div>
                <div class="form-group">
                    <label for="name">Dikirim Kepada</label>
                    <input type="text" name="name" id="name" class="form-control fc_md" required placeholder="Cari Customer">
                </div>
                <div class="form-group">
                    <label for="name">Dikirim Dari</label>
                    <select name="product_category_id" id="product_category_id" class="form-control fc_md select2" required>
                        <option>-- Pilih Lokasi Pengiriman --</option>
                        <option value="a_to_z">Urutkan A-Z</option>
                        <option value="z_to_a">Urutkan Z-A</option>
                        <option value="lowest">Harga Termurah - Termahal</option>
                        <option value="most_expensive">Harga Termahal - Termurah</option>
                        <option value="smallest">Stok Paling Sedikit</option>
                        <option value="biggest">Stok Paling Banyak</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Tanggal Pesan</label>
                    <input type="text" name="name" id="name" class="form-control fc_md" required>
                </div>
                
                <div class="form-group">
                    <label for="description">Catatan</label>
                    <textarea name="description" id="description" rows="3" class="form-control"></textarea>

                    <input type="checkbox" id="add_print" checked> <label for="add_print" class="font-weight-normal text-sm text-muted">TAMBAHKAN LABEL PRINT</label>
                </div>
            </form>
        @endcard
    </div>

    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <input type="text" class="form-control fc_md" placeholder="Cari Produk..">
            </div>
        </div>

        <div class="card card-border card-orange">
            <div class="card-header">
                <p class="mb-0">ORDERAN</p>
            </div>
            
            <div class="card-body">
                <table width="100%">
                    <tr class="row_header">
                        <td width="40%">NAMA</td>
                        <td width="20%">HARGA</td>
                        <td>QTY</td>
                        <td class="text-center">SUBTOTAL</td>
                    </tr>
                    <tr class="row_content">
                        <td>Mie Goreng</td>
                        <td>Rp. 2.500</td>
                        <td>3</td>
                        <td class="text-right">Rp. 7.500 
                            <a class="text-sm text-muted ml-3" href="{{ __('') }}"><i class="fas fa-ellipsis-h"></i></a>
                        </td>
                    </tr>
                </table>
                <hr>

                <table width="100%">
                    <tr>
                        <td width="40%">Subtotal</td>
                        <td width="20%"></td>
                        <td>
                            <strong>1</strong>
                        </td>
                        <td class="text-right">
                            <strong>Rp. 2.500</strong>
                            <a style="visibility: hidden;" class="text-sm text-muted ml-3" href="{{ __('') }}"><i class="fas fa-ellipsis-h"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Ongkos Kirim 
                            <strong>(0.2 Kg) -</strong>
                        </td>
                        <td></td>
                        <td></td>
                        <td class="text-right">
                            <strong>Rp. 2.000</strong>
                            <a style="visibility: hidden;" class="text-sm text-muted ml-3" href="{{ __('') }}"><i class="fas fa-ellipsis-h"></i></a>
                        </td>
                    </tr>
                </table>

                <div class="row mt-3">
                    <div class="col-12">
                        <button class="btn btn-outline-success"><i class="fas fa-plus-circle"></i> DISKON ORDER</button>
                        <button class="btn btn-outline-success"><i class="fas fa-plus-circle"></i> BIAYA LAIN</button>
                    </div>                
                </div>
                <hr>
                <div class="row mt-3">
                    <div class="col-6">
                        <h5 class="card-title">TOTAL</h5>
                    </div>
                    <div class="col-6 text-right">
                        <h2 class="font-weight-bold">Rp. 2.500</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-border card-orange">
            <div class="card-header">
                <h5 class="card-title">PEMBAYARAN</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Status Pembayaran</label>
                            <select name="" id="" class="form-control fc_md">
                                <option value="">Belum Bayar</option>
                                <option value="">Sudah Bayar (Lunas)</option>
                                <option value="">Cicilan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Bank Pembayaran</label>
                            <select name="" id="" class="form-control fc_md">
                                <option value="">Bank xxxx-xxxx-xxxx</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Tanggal Bayar</label>
                            <input type="text" class="form-control fc_md" placeholder="31 Oktober 2019">
                        </div>
                        <div class="form-group">
                            <label for="">Nominal</label>
                            <input type="text" class="form-control fc_md" placeholder="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-border card-orange">
            <div class="card-header">
                <h5 class="card-title">SHIPPING</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="">Nomor Resi</label>
                            <input type="text" class="form-control fc_md">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="" style="visibility: hidden;">Please, hide Me.</label>
                            <div class="form-control fc_md">
                                <label for="customSwitch1" class="font-weight-normal d-inline-block mb-1">Kirim SMS <strong>RESI</strong> otomatis</label>
                                <div class="custom-control custom-switch d-inline-block float-right">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                                    <label class="custom-control-label" for="customSwitch1"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
    <script>
        $(function () {
            $('select').select2()
        })
    </script>
@endpush