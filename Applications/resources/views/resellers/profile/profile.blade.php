@extends('resellers.partials.layouts.index')

@section('title', 'Profile')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profile</h1>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h4>Ganti Password</h4>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            Selalu ingat password anda
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <form action="{{ route('reseller.profile.change.password.update') }}" class="form-password" method="POST">
                                    @csrf @method('PUT')

                                    <div class="form-group">
                                        <label for="password" class="d-block">Password Lama *</label>
                                        <input id="password" type="password" name="password" class="form-control pwstrength" required>
                                    </div>
                    
                                    <div class="form-group">
                                        <label for="password2" class="d-block">Password Baru *</label>
                                        <input id="password2" type="password" name="new_password" class="form-control pwstrength" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password3" class="d-block">Konfirmasi Password Baru *</label>
                                        <input id="password3" type="password" name="password_confirmation" class="form-control" required>
                                    </div>

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary btn-password">Ubah Password</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <form action="{{ route('reseller.profile.update') }}" class="form-profile" method="POST">
                                    @csrf @method("PUT")

                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input id="email" value="{{ $reseller->email }}" type="email" class="form-control" name="email" required>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="first_name">Nama depan *</label>
                                            <input id="first_name" value="{{ $reseller->first_name }}" type="text" class="form-control" name="first_name" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="last_name">Nama belakang *</label>
                                            <input id="last_name" value="{{ $reseller->last_name }}" type="text" class="form-control" name="last_name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Alamat *</label>
                                        <textarea name="address" class="form-control" id="address" cols="30" rows="100" required>{{ $reseller->address }}</textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="city">Kota *</label>
                                            <input id="city" value="{{ $reseller->city }}" type="text" class="form-control" name="city" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="postal_code">Kode Pos *</label>
                                            <input id="postal_code" value="{{ $reseller->postal_code }}" type="number" class="form-control" name="postal_code" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">No. Telepon *</label>
                                        <input id="phone" type="number" value="{{ $reseller->phone }}" class="form-control" name="phone" required>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label>Nama Bank *</label>
                                            <input type="text" name="bank_name" value="{{ $reseller->bank_name }}" class="form-control" required>
                                        </div>
                                        <div class="form-group col-4">
                                            <label>No. Rekening *</label>
                                            <input type="text" name="bank_account" value="{{ $reseller->bank_account }}" class="form-control" required>
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Nama di Rekening *</label>
                                            <input type="text" name="bank_account_name" value="{{ $reseller->bank_account_name }}" class="form-control" required>
                                        </div>
                                    </div>
                    
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label>Nama Suami/Istri *</label>
                                            <input type="text" name="partners_name" value="{{ $reseller->partners_name }}" class="form-control" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Alamat Kantor (optional)</label>
                                            <input type="text" name="office_address" value="{{ $reseller->office_address }}" class="form-control">
                                        </div>
                                    </div>
                
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label>No. telepon recruiter *</label>
                                            <input type="text" name="phone_recruiter" value="{{ $reseller->phone_recruiter }}" class="form-control" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Email recruiter (optional)</label>
                                            <input type="text" name="email_recruiter" value="{{ $reseller->email_recruiter }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary btn-profile">Simpan Perubahan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $(".btn-password").click(function(e){
        e.preventDefault();

        var form_action = $("form.form-password").attr("action");

        // data form
        var password = $("input[name='password']").val();
        var new_password = $("input[name='new_password']").val();
        var password_confirmation = $("input[name='password_confirmation']").val();

        // reset
        $("input[name='password']").removeClass('is-invalid')
        $("input[name='new_password']").removeClass('is-invalid')
        $("input[name='password_confirmation']").removeClass('is-invalid')

        $(".err-password").remove();
        $(".err-new_password").remove();
        $(".err-password_confirmation").remove();

        $.ajax({
            dataType: 'json',
            type:'PUT',
            url: form_action,
            data:{
                _token: '{{ csrf_token() }}',
                password: password,
                new_password: new_password,
                password_confirmation: password_confirmation,
            },
        }).done(function(data){
            $("input[name='password']").val('');
            $("input[name='new_password']").val('');
            $("input[name='password_confirmation']").val('');

            _toast(data.message)
        }).fail(function(err){
            // _toast(err.responseJSON.message, 'warning', 2000)
            $.each(err.responseJSON, function (i, error) {
                var el = $("input[name='"+ i +"']");

                el.addClass('is-invalid');
                el.after($('<div class="err-' + i + ' invalid-feedback">' + error[0] + '</div>'));
            });
        });
    })

    $(".btn-profile").click(function(e){
        e.preventDefault();

        var form_action = $("form.form-profile").attr("action");

        // data form
        var email = $("input[name='email']").val();
        var first_name = $("input[name='first_name']").val();
        var last_name = $("input[name='last_name']").val();
        var address = $("textarea[name='address']").val();
        var city = $("input[name='city']").val();
        var postal_code = $("input[name='postal_code']").val();
        var phone = $("input[name='phone']").val();
        var bank_name = $("input[name='bank_name']").val();
        var bank_account = $("input[name='bank_account']").val();
        var bank_account_name = $("input[name='bank_account_name']").val();
        var partners_name = $("input[name='partners_name']").val();
        var office_address = $("input[name='office_address']").val();
        var phone_recruiter = $("input[name='phone_recruiter']").val();
        var email_recruiter = $("input[name='email_recruiter']").val();

        // reset
        $("input[name='email']").removeClass('is-invalid')
        $("input[name='first_name']").removeClass('is-invalid')
        $("input[name='last_name']").removeClass('is-invalid')
        $("input[name='address']").removeClass('is-invalid')
        $("input[name='city']").removeClass('is-invalid')
        $("input[name='postal_code']").removeClass('is-invalid')
        $("input[name='phone']").removeClass('is-invalid')
        $("input[name='bank_name']").removeClass('is-invalid')
        $("input[name='bank_account']").removeClass('is-invalid')
        $("input[name='bank_account_name']").removeClass('is-invalid')
        $("input[name='partners_name']").removeClass('is-invalid')
        $("input[name='office_address']").removeClass('is-invalid')
        $("input[name='phone_recruiter']").removeClass('is-invalid')
        $("input[name='email_recruiter']").removeClass('is-invalid')

        $(".err-email").remove();
        $(".err-first_name").remove();
        $(".err-last_name").remove();
        $(".err-address").remove();
        $(".err-city").remove();
        $(".err-postal_code").remove();
        $(".err-phone").remove();
        $(".err-bank_name").remove();
        $(".err-bank_account").remove();
        $(".err-bank_account_name").remove();
        $(".err-partners_name").remove();
        $(".err-office_address").remove();
        $(".err-phone_recruiter").remove();
        $(".err-email_recruiter").remove();

        $.ajax({
            dataType: 'json',
            type:'PUT',
            url: form_action,
            data:{
                _token: '{{ csrf_token() }}',
                email: email,
                first_name: first_name,
                last_name: last_name,
                address: address,
                city: city,
                postal_code: postal_code,
                phone: phone,
                bank_name: bank_name,
                bank_account: bank_account,
                bank_account_name: bank_account_name,
                partners_name: partners_name,
                office_address: office_address,
                phone_recruiter: phone_recruiter,
                email_recruiter: email_recruiter,
            },
        }).done(function(data){
            _toast('Profile anda berhasil diubah')
        }).fail(function(err){
            _toast('Terjadi kesalahan, periksa kembali form anda', 'warning', 2000)

            if(err.responseJSON['street_address'] != undefined) {
                var street = $("textarea[name='address']")
                street.addClass('is-invalid');
                street.after($('<div class="err-address invalid-feedback">' + err.responseJSON['address'][0] + '</div>'));
            }

            $.each(err.responseJSON, function (i, error) {
                var el = $("input[name='"+ i +"']");

                el.addClass('is-invalid');
                el.after($('<div class="err-' + i + ' invalid-feedback">' + error[0] + '</div>'));
            });
        });
    })
})
</script>
@endpush