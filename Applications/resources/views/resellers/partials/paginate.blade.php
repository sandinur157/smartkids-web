@foreach ($products as $product)
    <div class="col-6 col-md-4 col-lg-4">
        <article class="article article-style-c">
            <a href="{{ route('reseller.shop.show.get', $product->id) }}">
                <div class="article-header">
                    <div class="article-image" style="background-image: url('{{asset('images/reseller/placeholder.png')}}')">
                        <img id="image-product" src="{{ asset('images/products/' . $product->photo) }}" style="width: 100%; height: 100%; object-fit: cover">
                    </div>
                </div>
                <div class="article-details">
                    <div class="article-title">
                        <h2 class="text-center">
                            <a href="{{ route('reseller.shop.show.get', $product->id) }}">{{ $product->name }}</a>
                        </h2>
                    </div>
                    <div class="article-user text-center">
                        <div class="article-user-details">
                            <div class="text-success font-weight-bold">@currency($product->reseller_price)</div>
                        </div>
                    </div>
                </div>
            </a>
        </article>
    </div>
@endforeach

{{ $products->links() }}