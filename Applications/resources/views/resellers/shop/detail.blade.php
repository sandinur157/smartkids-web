@extends('resellers.partials.layouts.indexShop')

@section('title', 'Produk')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/resellers/inputnumber.css') }}">
@endpush

@section('content')
    <section class="section mt-4">
        <div class="row mb-3">
            <div class="col-12 col-md-6 col-lg-6">
                <img src="{{ asset('images/products/' . $product->photo) }}" class="img-single-product" alt="{{ $product->name }}">
            </div>

            <div class="col-12 col-md-6 col-lg-6">
                <h1 class="text-dark">{{ strtoupper($product->name) }}</h1>
                <h3 class="text-success mt-2">@currency($product->nominal_price)</h3>

                <form action="{{ route('reseller.cart.post') }}" method="POST">
                    @csrf @method("POST")
                    
                    <div class="mt-4 mb-3 text-success">
                        <h6><i class="fa fa-check" aria-hidden="true"></i> {{ $product->product_stock->amount }} Stok</h6>
                    </div>

                    <div class="form-group">
                        <label for="qty">Quantity</label> <br>
                        <span class="input-number-decrement">–</span><input class="input-number" id="qty" type="text" value="1" min="1" max="{{ $product->product_stock->amount }}"><span class="input-number-increment">+</span>
                    </div>

                    {{-- <span>
                        <strong class="text-dark">Category:</strong>
                        {{ $product->product_category->name }}
                    </span> --}}


                    @if(Auth::guard('reseller')->check())
                        <button type="submit" id="btn-cart" class="btn btn-primary btn-lg btn-block btn-icon-split mt-3">
                            Masukkan Keranjang
                        </button>
                    @else
                        <a href="{{ route('reseller.login.get') }}" class="btn btn-primary btn-lg btn-block btn-icon-split mt-3">
                            Login Untuk Memesan
                        </a>
                    @endif
                </form>
            </div>
        </div>

        {!! $product->description !!}
    </section>
@endsection

@push('scripts')
    <script src="{{ asset('js/resellers/inputnumber.js') }}"></script>
    <script>
        var loading = false;

        $('#btn-cart').on("click", function(e){
            e.preventDefault();
            loading = true;

            var qty = $("#qty").val();

            $.ajax({
                type:'POST',
                url: '{{ route("reseller.cart.post") }}',
                data:{
                    _token: '{{ csrf_token() }}',
                    id_product: {{ $product->id }},
                    qty: qty,
                },
            }).done(function(data){
                loading = false;
                console.log(data);

                $("#cart-list").html(data);

                var total = $("#total").text();
                $(".txt-total").empty().append(total);

                _toast('Berhasil ditambahkan ke keranjang')
            }).fail(function(err){
                console.log(err)
                loading = false;
                
                if(err.status == 422 || err.status == 404) {
                    alert(err.responseJSON.message);
                }else{
                    alert("Terjadi kesalahan, Reload browser anda");
                }

            });
        });
    </script>
@endpush