@extends('resellers.partials.layouts.indexShop')

@section('title', 'Checkout')

@section('content')
    <section class="section mt-4">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Order</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-md">
                                <tbody>
                                    <tr>
                                        <th>Produk</th>
                                        <th>Harga</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                    @php
                                        $total_items = 0;
                                        $subtotal = 0;
                                    @endphp

                                    @if (count($carts) != 0)
                                        @foreach ($carts as $item)    
                                            @php
                                                $total_items += $item->qty;
                                                $subtotal += $item->total;
                                            @endphp
                                            <tr>
                                                <td>{{ $item->product->name }}</td>
                                                <td>{{ $item->product->nominal_price }}</td>
                                                <td>{{ $item->qty }}</td>
                                                <td>{{ $item->total }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">
                                                Keranjang masih kosong
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td colspan="2"><h5>Total</h5></td>
                                        <td colspan="2" class="text-success"><h5>@currency($subtotal)</h5></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Detail Penagihan</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reseller.checkout.post') }}" method="post" class="needs-validation">
                            @csrf @method('POST')

                            @if (count($carts) != 0)
                                <div>
                                    <label for="" class="text-dark"><strong>Alamat penagihan</strong></label>
                                    <p>
                                        {{ $billing_address->first_name . " " . $billing_address->last_name }} <br>
                                        {{ $billing_address->street_address }} <br>
                                        {{ $billing_address->phone }}
                                    </p>
                                </div>
                                <a href="{{ route('reseller.address.billing.get') }}" class="btn btn-primary mb-3 float-right">Ubah alamat penagihan</a>

                                <div class="clearfix"></div>

                                <input type="hidden" name="total_item" value="{{ $total_items }}">
                                <input type="hidden" name="total_price" value="{{ $subtotal }}">

                                <div class="form-group">
                                    <label for="note">Catatan Order (optional)</label>
                                    <textarea name="note" class="form-control" id="notes" cols="30" rows="100" style="height: 80px"></textarea>
                                </div>

                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-primary">Pesan Tempat</button>
                                </div>
                            @else
                                <a href="{{ route('reseller.shop.get') }}" class="btn btn-primary btn-block">Belanja</a>
                            @endif
                        </form>
                    </div>
                    <div class="card-footer text-right">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection