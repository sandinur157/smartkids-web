@modal
    @slot('title', '')
    
    <form method="post" class="needs-validation" novalidate>
        @csrf @method('post')
        <input type="hidden" name="id" id="id">
        <div class="form-group">
            <label for="product_id">Produk</label>
            <select name="product_id" id="product_id" class="custom-select custom-select-sm">
                <option disabled> -- Isi Produk --</option>
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">
                Pilih produk yang ingin ditambahkan atribut.
            </div>
        </div>
        <div class="form-group">
            <label for="name">Nama Atribut</label>
            <input type="text" name="name" id="name" class="form-control" required autofocus>
            <div class="invalid-feedback">
                Isi Atribut dengan benar (min 4 digit).
            </div>
        </div>

        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal