function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

// CHANGE THEME
$("document").ready(function(){
    var color = getCookie("theme");
    theme(color);
});

function theme (color) {
    var classTheme = ["theme-blue-dark", "theme-blue-light", "theme-green", "theme-orange", "theme-red"];
    var elementTheme = [".navbar-bg", ".nav-pills .nav-item .nav-link.active", ".page-item.active .page-link", ".page-link:hover", ".btn-primary, .btn-primary.disabled"];
    var classThemeFont = ["theme-blue-dark-font", "theme-blue-light-font", "theme-green-font", "theme-orange-font", "theme-red-font"];
    var fontTheme = [".card .card-header h4", '.nav-pills .nav-item .nav-link:not(".nav-pills .nav-item .nav-link.active")'];

    // RESET
    for(var i = 0; i < classTheme.length; i++) {
        for(var j = 0; j < elementTheme.length; j++) {
            $(elementTheme[j]).removeClass(classTheme[i]);
        }
    }
    
    for(var i = 0; i < classThemeFont.length; i++) {
        for(var j = 0; j < fontTheme.length; j++) {
            $(fontTheme[j]).removeClass(classThemeFont[i]);
        }
    }

    // HOVER COLOR
    var hoverColor = (function(color) {
        $(".page-item .page-link").mouseenter(function() {
            $(this).css("background", `#${color}`);
        }).mouseleave(function() {
            $(this).css("background", "#f9fafe");
        });
    })

    // Background Color
    for(var i = 0; i < elementTheme.length; i++) {
        if (color == 1) { 
            hoverColor("6777ef");
            $(elementTheme[i]).addClass("theme-blue");
        }else if (color == 2) {
            $(elementTheme[i]).addClass("theme-blue-dark");
            hoverColor("34395e");
        }else if (color == 3) {
            $(elementTheme[i]).addClass("theme-blue-light");
            hoverColor("3abaf4");
        }else if (color == 4) {
            $(elementTheme[i]).addClass("theme-green");
            hoverColor("47c363");
        }else if (color == 5) {
            $(elementTheme[i]).addClass("theme-orange");
            hoverColor("ffa426");
        }else if (color == 6) {
            $(elementTheme[i]).addClass("theme-red");
            hoverColor("fc544b");
        }
    }

    // Color
    for(var i = 0; i < fontTheme.length; i++) {
        if (color == 1) { 
            $(fontTheme[i]).addClass("");
        }else if (color == 2) {
            $(fontTheme[i]).addClass("theme-blue-dark-font");
        }else if (color == 3) {
            $(fontTheme[i]).addClass("theme-blue-light-font");
        }else if (color == 4) {
            $(fontTheme[i]).addClass("theme-green-font");
        }else if (color == 5) {
            $(fontTheme[i]).addClass("theme-orange-font");
        }else if (color == 6) {
            $(fontTheme[i]).addClass("theme-red-font");
        }
    }
}

function themeClick(color) {
    theme(color);
    document.cookie = "theme=" + color + "; path=/";
}


// SCROLL UP
$(window).ready(function() {
    if($("body").offset().top == 0) {
        $("#scroll-up").fadeOut();
    }
});

$(window).scroll(function() {
    if($(this).scrollTop() <= 215) {
        $("#scroll-up").fadeOut();
    } else {
        $("#scroll-up").fadeIn();
    }
});

$('#scroll-up').click(function (e) {
    $('html, body').animate({
        scrollTop: $('body').offset().top - 20
    }, 'ease-in');
});